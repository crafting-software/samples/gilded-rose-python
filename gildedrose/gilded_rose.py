# -*- coding: utf-8 -*-
from .item import Item
from .item_action import (
    ItemAction,
    case,
    decrease_quality,
    decrease_sell_in,
    expired_in_less_than,
    has_expired,
    increase_quality,
    nop,
    sequence,
    set_quality,
)

DEFAULT_UPDATER = sequence(
    decrease_quality(1), decrease_sell_in(1), case(has_expired(), decrease_quality(1))
)

AGED_BRIE_UPDATER = sequence(
    increase_quality(1), decrease_sell_in(1), case(has_expired(), increase_quality(1))
)

SULFURAS_UPDATER = nop()

BACKSTAGE_UPDATER = sequence(
    increase_quality(1),
    case(expired_in_less_than(10), increase_quality(1)),
    case(expired_in_less_than(5), increase_quality(1)),
    decrease_sell_in(1),
    case(has_expired(), set_quality(0)),
)

CONJURED_UPDATER = sequence(
    decrease_quality(2), decrease_sell_in(1), case(has_expired(), decrease_quality(2))
)


RULE_FOR = {
    "Aged Brie": AGED_BRIE_UPDATER,
    "Sulfuras, Hand of Ragnaros": SULFURAS_UPDATER,
    "Backstage passes to a TAFKAL80ETC concert": BACKSTAGE_UPDATER,
    "Conjured Mana Cake": CONJURED_UPDATER,
}


def get_rule_for(item: Item) -> ItemAction:
    if item.name in RULE_FOR:
        return RULE_FOR[item.name]
    return DEFAULT_UPDATER


class GildedRose:
    def __init__(self, items):
        self.items = items

    def update_quality(self):
        for item in self.items:
            get_rule_for(item)(item)
