# -*- coding: utf-8 -*-
from collections.abc import Callable

from .item import Item

MIN_QUALITY = 0
MAX_QUALITY = 50


# Using side effect, could be improved with imutable Item
ItemAction = Callable[[Item], None]
ItemPredicate = Callable[[Item], bool]


def nop() -> ItemAction:
    def run(_: Item) -> None:
        pass

    return run


def decrease_quality(value: int) -> ItemAction:
    if value < 0:
        raise ValueError("cannot decrease quality with negative value")

    def run(item: Item) -> None:
        item.quality = max(MIN_QUALITY, item.quality - value)

    return run


def increase_quality(value: int) -> ItemAction:
    if value < 0:
        raise ValueError("cannot increase quality with negative value")

    def run(item: Item) -> None:
        item.quality = min(MAX_QUALITY, item.quality + value)

    return run


def set_quality(value: int) -> ItemAction:
    if value < 0 or value > 50:
        raise ValueError("cannot set quality with value outside [0; 50]")

    def run(item: Item) -> None:
        item.quality = value

    return run


def decrease_sell_in(value: int) -> ItemAction:
    if value < 0:
        raise ValueError("cannot decrease sellin with negative value")

    def run(item: Item) -> None:
        item.sell_in = item.sell_in - value

    return run


def has_expired() -> ItemPredicate:
    def check(item) -> bool:
        return item.sell_in <= 0

    return check


def expired_in_less_than(days: int) -> ItemPredicate:
    if days < 0:
        raise ValueError("cannot check expiry of negative value")

    def check(item) -> bool:
        return item.sell_in <= days

    return check


def sequence(*actions: ItemAction) -> ItemAction:
    def run(item: Item) -> None:
        for action in actions:
            action(item)

    return run


def case(predicate: ItemPredicate, action: ItemAction) -> ItemAction:
    def run(item: Item) -> None:
        if predicate(item):
            action(item)

    return run
