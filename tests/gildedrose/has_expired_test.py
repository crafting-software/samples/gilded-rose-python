# -*- coding: utf-8 -*-
from hypothesis import given
from hypothesis.strategies import integers

from gildedrose.item import Item
from gildedrose.item_action import has_expired


@given(integers())
def test_has_expired(sell_in):
    assert has_expired()(Item("", sell_in, 20)) == (sell_in <= 0)
