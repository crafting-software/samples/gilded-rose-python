# -*- coding: utf-8 -*-
from hypothesis import given
from hypothesis.strategies import integers
from pytest import raises

from gildedrose.item_action import set_quality

from .action_helper import update_quality


@given(integers(min_value=1))
def test_set_quality_with_negatives_values(amount):
    with raises(ValueError):
        set_quality(-amount)


@given(integers(min_value=51))
def test_set_quality_with_values_grether_than_50(amount):
    with raises(ValueError):
        set_quality(amount)


@given(integers(min_value=0, max_value=50), integers(min_value=0, max_value=50))
def test_set_quality(initial, value):
    assert update_quality(initial, set_quality(value)) == value
