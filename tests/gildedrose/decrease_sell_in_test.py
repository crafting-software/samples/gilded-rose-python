# -*- coding: utf-8 -*-
from hypothesis import given
from hypothesis.strategies import integers
from pytest import raises

from gildedrose.item_action import decrease_sell_in

from .action_helper import update_sell_in


@given(integers(min_value=1))
def test_cannot_decrease_negative_values(amount):
    with raises(ValueError):
        decrease_sell_in(-amount)


@given(integers())
def test_decrease_sell_in(sell_in):
    assert update_sell_in(sell_in, decrease_sell_in(1)) == sell_in - 1
