# -*- coding: utf-8 -*-
from gildedrose.item import Item
from gildedrose.item_action import ItemAction


def update_quality(initial: int, action: ItemAction) -> int:
    item = Item("", 10, initial)
    action(item)
    return item.quality


def update_sell_in(initial: int, action: ItemAction) -> int:
    item = Item("", initial, 20)
    action(item)
    return item.sell_in
