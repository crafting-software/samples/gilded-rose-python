# -*- coding: utf-8 -*-
from hypothesis import given
from hypothesis.strategies import integers
from pytest import raises

from gildedrose.item_action import decrease_quality

from .action_helper import update_quality


@given(integers(min_value=1))
def test_cannot_decrease_negative_values(amount):
    with raises(ValueError):
        decrease_quality(-amount)


@given(integers(min_value=1, max_value=50))
def test_decrease_quality_when_not_min(quality):
    assert update_quality(quality, decrease_quality(1)) == quality - 1


@given(integers(min_value=1, max_value=50), integers(min_value=0))
def test_decrease_quality_should_be_valid_quality(quality, amount):
    assert update_quality(quality, decrease_quality(amount)) >= 0
    assert update_quality(quality, decrease_quality(amount)) <= 50
