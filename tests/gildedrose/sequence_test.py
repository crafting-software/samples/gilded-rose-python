# -*- coding: utf-8 -*-
from hypothesis import given
from hypothesis.strategies import integers

from gildedrose.item_action import decrease_quality, increase_quality, sequence

from .action_helper import update_quality


@given(integers(min_value=1, max_value=50))
def test_empty_sequence_is_no_op(quality):
    assert update_quality(quality, sequence()) == quality


@given(integers(min_value=1, max_value=50))
def test_sequence_of_operation(quality):
    action = sequence(decrease_quality(1), increase_quality(1))
    assert update_quality(quality, action) == quality
