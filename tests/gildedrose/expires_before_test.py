# -*- coding: utf-8 -*-
from hypothesis import given
from hypothesis.strategies import integers
from pytest import raises

from gildedrose.item import Item
from gildedrose.item_action import expired_in_less_than


@given(integers(min_value=1))
def test_cannot_call_with_negative_value(amount):
    with raises(ValueError):
        expired_in_less_than(-amount)


@given(integers(), integers(min_value=0))
def test_expired_in_less_than(sell_in, days):
    assert expired_in_less_than(days)(Item("", sell_in, 20)) == (sell_in <= days)
