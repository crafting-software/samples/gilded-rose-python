# -*- coding: utf-8 -*-
from hypothesis import given
from hypothesis.strategies import integers
from pytest import raises

from gildedrose.item_action import increase_quality

from .action_helper import update_quality


@given(integers(min_value=1))
def test_cannot_increase_negative_values(amount):
    with raises(ValueError):
        increase_quality(-amount)


@given(integers(min_value=0, max_value=49))
def test_increase_quality_when_not_max(quality):
    assert update_quality(quality, increase_quality(1)) == quality + 1


@given(integers(min_value=1, max_value=50), integers(min_value=0))
def test_increase_quality_should_be_valid_quality(quality, amount):
    assert update_quality(quality, increase_quality(amount)) >= 0
    assert update_quality(quality, increase_quality(amount)) <= 50
