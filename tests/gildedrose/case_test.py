# -*- coding: utf-8 -*-
from hypothesis import given
from hypothesis.strategies import integers

from gildedrose.item_action import case, decrease_quality

from .action_helper import update_quality


@given(integers(min_value=1, max_value=50))
def test_case_should_not_call_action_when_predicate_is_false(quality):
    action = case(lambda _: False, decrease_quality(1))
    assert update_quality(quality, action) == quality


@given(integers(min_value=1, max_value=50))
def test_case_call_action_when_predicate_is_true(quality):
    action = case(lambda _: True, decrease_quality(1))
    assert update_quality(quality, action) == quality - 1
